#include <complex.h>
#include <cmath>
#include <fstream>
#include <stdio.h>
#include <iostream>
#include <stdlib.h>
#include <cstdlib>
#include <stdio.h>

using namespace std;

//Dimensionalities of the spaces we are dealing with
#define dSpin 2
#define dElem 2
#define dOrb 5
#define dSublat 3
#define N1 4
#define N2 4
#define N3 4
#define Ntot 64

//Number of columms of the matrices
#define nCols 60

typedef complex<double> dcmplx;

///////
////Class for setting a point and returning any of the coordinates. This class also finds what these x, y, and z coordinates should be, given a set of fractional coordinates and the primitive lattice vectors.
class Point
{
	public:
		Point(double, double, double);
		Point(Point, Point);
    Point(int, int, int, double[3][3]);
		Point(int, int, double[6][3], double[3][3]);
		double point[3];
		double coordsList[3];
		void setPoint(double x, double y, double z){point[0] = x; point[1] = y; point[2] = z;};
		double divide(int num1, int num2){return (double) num1/num2;};
    double getCoord(int ind){return point[ind];};
		double getFractionalCoordinate(int ind) {return coordsList[ind];};
		void findPoint(double coords[3],double prim[3][3]);
		void printPoint(){cout << "point is: {" << point[0] << ", " << point[1] << ", " << point[2] << "}" << endl;};
		void pointDifference(Point pointA, Point pointB){setPoint(pointA.getCoord(0)-pointB.getCoord(0),pointA.getCoord(1)-pointB.getCoord(1),pointA.getCoord(2)-pointB.getCoord(2));};
		int sublatticeMapping(int, int);
    void setCoords(int, double[6][3]);
};

Point::Point(double x, double y, double z)
{
	setPoint(x,y,z);
}

Point::Point(Point pointA, Point pointB)
{
	pointDifference(pointA,pointB);
}

Point::Point(int n1, int n2, int n3, double recip[3][3])
{
    double total[3] = {0, 0, 0};
    double coords[3] = {divide(n1,N1), divide(n2,N2), divide(n3,N3)};

    for(int j = 0; j < 3; j++)
    {
      for(int i = 0; i < 3; i++)
      {
          total[i] += coords[j]*recip[j][i];
      }
    }

    setPoint(total[0],total[1],total[2]);
}

Point::Point(int elem, int sublat, double subArray[6][3], double prim[3][3])
{
    double total[3] = {0, 0, 0};
    for(int i = 0; i < 3; i++)
    {
      coordsList[i] = subArray[sublatticeMapping(elem, sublat)][i];
    }

    for(int j = 0; j < 3; j++)
		{
			for(int i = 0; i < 3; i++)
    	{
      	total[i] += coordsList[j]*prim[j][i];
    	}
		}

    setPoint(total[0],total[1],total[2]);
}

//This part finds the point (in Cartesian) given fractional coordinates and the primitive vectors
void Point::findPoint(double coords[3], double prim[3][3])
{
        double total[3] = {0, 0, 0};
        for(int j = 0; j < 3; j++)
        {
        	for(int i = 0; i < 3; i++)
					{
						total[i] += coords[j]*prim[j][i];
					}
				}

					setPoint(total[0], total[1], total[2]);
}

int Point::sublatticeMapping(int elem, int sublat)
{
        return elem * 3 + sublat;
}

void Point::setCoords(int ind, double subArray[6][3])
{
    for(int i = 0; i < 3; i++)
    {
        coordsList[i] = subArray[ind][i];
    }
}
