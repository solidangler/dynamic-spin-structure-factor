//This is the version of the code where I have reverted to hard-coding the file names. I have removed the Ntot and replaced them with 27 for the 3x3x3 case.

#include "spinStructureFactorFunctions.cpp"

int main(int argc, char* argv[])
{
//Here I define the sublattice fractional coordinates. The first 3 are Cr, the last 3 are Fe.
double sublattice[6][3] = {{.4146, .4146, 0.5}, {1, .5854, 0.5}, {.5854, 0.0, 0.5}, {0.0, 0.2485,
  0.0}, {0.2485, 0.0, 0.0}, {0.7515, 0.7515, 0.0}};

//Lattice Vectors and reciprocal lattice vectors
double a = 6.0675, c = 3.6570;
double primvecs[3][3] = {{a, 0, 0}, {-.5*a, sqrt(3)*a/2, 0}, {0, 0, c}};
double recVectors[3][3] = {{1.03555, 0.597874, 0.00}, {0.00, 1.19575, 0.00}, {0.0, 0.0, 1.71813}};

int belowLengths[Ntot] = {36, 33, 34, 33, 35, 32, 33, 32, 34, 32, 33, 32, 35, 32, 34, 32, 35, 32, 33, 32, 33, 33, 33, 32, 33, 33, 33, 32, 35, 32, 34, 32, 34, 32, 33, 32, 34, 33, 33, 32, 34, 32, 33, 32, 33, 32, 33, 33, 35, 32, 34, 32, 35, 32, 33, 32, 34, 32, 33, 33, 33, 32, 33, 33};

int aboveLengths[Ntot] = {24, 27, 26, 27, 25, 28, 27, 28, 26, 28, 27, 28, 25, 28, 26, 28, 25, 28, 27, 28, 27, 27, 27, 28, 27, 27, 27, 28, 25, 28, 26, 28, 26, 28, 27, 28, 26, 27, 27, 28, 26, 28, 27, 28, 27, 28, 27, 27, 25, 28, 26, 28, 25, 28, 27, 28, 26, 28, 27, 27, 27, 28, 27, 27};

int qpoints[13][3] = {{0,0,0},{1,0,0},{2,0,0},{3,0,0},{4,0,0},{5,0,0},{6,0,0},{7,0,0},{8,0,0},{9,0,0},{10,0,0},{11,0,0},{12,0,0}};

int belowInfo[Ntot][60];
int aboveInfo[Ntot][60];

//Matrix to store eigensystem information
dcmplx matrix1[Ntot][60][60];
dcmplx matrix2[Ntot][60][60];
double eigenvals[Ntot][60];

//Numbers that are used to load the above matrices
double real;
double imag;
double ev;

//Import matrix data
FILE * evecsFile;
FILE * evalsFile;
FILE * belowFile;
FILE * aboveFile;

char evecFileName[40];
char evalFileName[40];
char belowFileName[40];
char aboveFileName[40];

sprintf(evecFileName,"%dx%dx%d/hamiltonianData.txt",N1,N2,N3);
sprintf(evalFileName,"%dx%dx%d/eigenvalueData.txt",N1,N2,N3);
sprintf(belowFileName,"%dx%dx%d/belowData.txt",N1,N2,N3);
sprintf(aboveFileName,"%dx%dx%d/aboveData.txt",N1,N2,N3);

evecsFile = fopen(evecFileName,"r");
evalsFile = fopen(evalFileName,"r");
belowFile = fopen(belowFileName,"r");
aboveFile = fopen(aboveFileName,"r");

readBelowData(belowFile, belowInfo, belowLengths);
readBelowData(aboveFile, aboveInfo, aboveLengths);

//Load eigenvalue data into memory
for(int kpoint = 0; kpoint < N1 * N2 * N3 ; kpoint++)
{
        for(int i = 0; i < 60; i++)
        {
                fscanf(evalsFile, "%lf\n" , &ev);
                eigenvals[kpoint][i] = ev;
        }
}

//Load eigenvector information
for(int kpoint = 0; kpoint < N1 * N2 * N3 ; kpoint++)
{
        for(int i = 0; i < 3600; i++)
        {
                fscanf(evecsFile,"(%lf,%lf)\n",&real,&imag);
                matrix1[kpoint][row(i)][column(i)] = dcmplx(real,imag);
                matrix2[kpoint][column(i)][row(i)] = conj(dcmplx(real,imag));
        }
}

cout << "The total number of unit cells is: " << Ntot << endl;

// //Now I just need to apply the primitive vectors.
FILE * outputFile;
char buffer[20];

cout << "Printing to file................. " <<  endl;
for(int qpts = 0; qpts < 13; qpts++)
{
  sprintf(buffer ,"output%d.txt", qpts);
  outputFile = fopen(buffer,"w");

  for (int i = 0; i < N1; i++)
  {
    for (int j = 0; j < N2; j++)
    {
      for (int k = 0; k < N3; k++)
      {
        tableLambdas(outputFile, matrix1, matrix2, eigenvals, sublattice, primvecs, recVectors, belowInfo, aboveInfo, belowLengths, aboveLengths, qpts, qpoints[qpts][0], qpoints[qpts][1], qpoints[qpts][2], i, j, k);
      }
    }
  }
}

cout << Ntot << endl;

cout << "Done printing to file!!" << endl;

return 0;
}
