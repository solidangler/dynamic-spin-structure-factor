#include "pointClass.cpp"

int BZIndexMapping(int n1, int n2, int n3)
{
  return n1 * N2 * N3 + n2 * N3 + n3;
}

int indexMapper(int spin, int elem, int orb, int sublat)
{
  return spin * dSpin * dElem * dOrb * dSublat + elem * dOrb * dSublat + orb * dSublat + sublat;
}

double energy(double evals[27][60], int kpoint, int lam)
{
  return evals[kpoint][lam];
}

int foldedVec(int n1, int n2, int n3)
{
  return BZIndexMapping(n1%N1, n2%N2, n3%N3);
}

double energyDifference(double evals[27][60], int n1, int n2, int n3, int n1q, int n2q, int n3q, int lam, int lamb)
{
  return evals[foldedVec(n1 + n1q, n2 + n2q, n3 + n3q)][lamb] - evals[BZIndexMapping(n1, n2, n3)][lam];
}

double dot(double vec1[3],double vec2[3])
{
  return (vec1[0]*vec2[0] + vec1[1]*vec2[1] + vec1[2]*vec2[2]);
}

double deltaFunction(double x)
{
  return 1/(3.14159265 * (1 + (x*x)));
}

// void setPoints(int n1q, int n2q, int n3q, recip[3][3])
// {
//   Point qvec(n1q, n2q, n3q, recip);
//   cout << qvec.getCoord(0) << endl;
// }

dcmplx matrixElementGetter1(dcmplx matrix[27][60][60], int n1, int n2, int n3, int spin, int elem, int orb, int sublat, int lambda)
{
        dcmplx weight1 = matrix[BZIndexMapping(n1, n2, n3)][lambda][indexMapper(spin, elem, orb, sublat)];
        return weight1;
}

dcmplx matrixElementGetter2(dcmplx matrix[27][60][60], int n1, int n2, int n3, int spin, int elem, int orb, int sublat, int lambda)
{
        dcmplx weight2 = matrix[foldedVec(n1, n2, n3)][indexMapper(spin, elem, orb, sublat)][lambda];
        return weight2;
}

dcmplx matrixElementGetter3(dcmplx matrix[27][60][60], int n1, int n2, int n3, int spin, int elem, int orb, int sublat, int lambda)
{
        dcmplx weight3 = matrix[foldedVec(n1, n2, n3)][lambda][indexMapper(spin, elem, orb, sublat)];
        return weight3;
}

dcmplx matrixElementGetter4(dcmplx matrix[27][60][60], int n1, int n2, int n3, int spin, int elem, int orb, int sublat, int lambda)
{
        dcmplx weight4 = matrix[BZIndexMapping(n1, n2, n3)][indexMapper(spin, elem, orb, sublat)][lambda];
        return weight4;
}

dcmplx phase(Point qVector, Point differ)
{
  return exp(dcmplx(0,-1)*(dot(qVector.point, differ.point)));
}

int aboveBelowMapping(int info[27][60], int kpoint, int ind)
{
  return info[kpoint][ind];
}

dcmplx calculateFullTerm(dcmplx mat1[27][60][60], dcmplx mat2[27][60][60], double evals[27][60], double subArray[6][3], double prim[3][3], double recip[3][3], int belowInfob[27][60], int aboveInfob[27][60], int n1q, int n2q, int n3q, int n1, int n2, int n3, int spin, int elem, int orb, int sublat, int lambda, int spinp, int elemp, int orbp, int sublatp, int lambdap)
{
    Point qvec(n1q, n2q, n3q, recip);
    Point point1(elem, sublat, subArray, prim);
    Point point2(elemp, sublatp, subArray, prim);
    Point diff(point1, point2);
    dcmplx weight1 = matrixElementGetter1(mat2, n1, n2, n3, spin, elem, orb, sublat, lambda);
    dcmplx weight2 = matrixElementGetter2(mat1, n1 + n1q, n2 + n2q, n3 + n3q, spinp, elem, orb, sublat, lambdap);
    dcmplx weight3 = matrixElementGetter3(mat2, n1 + n1q, n2 + n2q, n3 + n3q, spinp, elemp, orbp, sublatp, lambdap);
    dcmplx weight4 = matrixElementGetter4(mat1, n1, n2, n3, spin, elemp, orbp, sublatp, lambda);
    dcmplx ph = phase(qvec, diff);
    double delta = deltaFunction(energy(evals, foldedVec(n1 + n1q, n2 + n2q, n3 + n3q), aboveInfob[foldedVec(n1 + n1q, n2 + n2q, n3 + n3q)][lambdap]) - energy(evals, BZIndexMapping(n1, n2, n3), belowInfob[BZIndexMapping(n1, n2, n3)][lambda]));
    return weight1*weight2*weight3*weight4*ph*((dcmplx) delta);
}

dcmplx sumAllTerms(dcmplx mat1[27][60][60], dcmplx mat2[27][60][60], double evals[27][60], double subArray[6][3], double prim[3][3], double recip[3][3], int belowInfob[27][60], int aboveInfob[27][60], int n1q, int n2q, int n3q, int n1, int n2, int n3, int lambda, int lambdap)
{
  dcmplx finalTerm = 0;

  for(int i4 = 0; i4 < 2; i4++)
  {
    for(int i5 = 0; i5 < 2; i5++)
    {
      for(int i6 = 0; i6 < 5; i6++)
      {
        for(int i7 = 0; i7 < 3; i7++)
        {
          for(int i11 = 0; i11 < 2; i11++)
          {
            for(int i12 = 0; i12 < 2; i12++)
            {
              for(int i13 = 0; i13 < 5; i13++)
              {
                for(int i14 = 0; i14 < 3; i14++)
                {
                  finalTerm += calculateFullTerm(mat1, mat2, evals, subArray, prim, recip, belowInfob, aboveInfob, n1q, n2q, n3q, n1, n2, n3, i4, i5, i6, i7, lambda, i11, i12, i13, i14, lambdap);
                }
              }
            }
          }
        }
      }
    }
  }

  return finalTerm;
}

void writeToFile(FILE * file, int nk1, int nk2, int nk3, double eng, int qNum, dcmplx weightFinal)
{
  fprintf(file, "%d %d %d %lf %d %lf %lf\n", nk1, nk2, nk3, eng, qNum, real(weightFinal), imag(weightFinal));
}

void writeData(FILE * file, dcmplx mat1[27][60][60], dcmplx mat2[27][60][60], double evals[27][60], double subArray[6][3], double prim[3][3], double recip[3][3], int belowInfob[27][60], int aboveInfob[27][60], int qNum, int n1, int n2, int n3, int n1q, int n2q, int n3q, int lambda, int lambdap)
{
  writeToFile(file, n1, n2, n3, energyDifference(evals, n1, n2, n3, n1q, n2q, n3q, lambda, lambdap), qNum, sumAllTerms(mat1, mat2, evals, subArray, prim, recip, belowInfob, aboveInfob, n1, n2, n3, n1q, n2q, n3q, lambda, lambdap));
}

void tableLambdas(FILE * file, dcmplx mat1[27][60][60], dcmplx mat2[27][60][60], double evals[27][60], double subArray[6][3], double prim[3][3], double recip[3][3], int belowInfob[27][60], int aboveInfob[27][60], int beLengths[27], int abLengths[27], int qNum, int n1, int n2, int n3, int n1q, int n2q, int n3q)
{
  for(int la = 0; la < beLengths[BZIndexMapping(n1, n2, n3)]; la++)
  {
    for(int lab = 0; lab < abLengths[foldedVec(n1 + n1q, n2 + n2q, n3 + n3q)]; lab++)
    {
      writeData(file, mat1, mat2, evals, subArray, prim, recip, belowInfob, aboveInfob, qNum, n1, n2, n3, n1q, n2q, n3q, la, lab);
    }
  }
}

// void printWeights()
// {
//   cout << "weight1: " << weight1 << endl;
//   cout << "weight2: " << weight2 << endl;
//   cout << "weight3: " << weight3 << endl;
//   cout << "weight4: " << weight4 << endl;
// }

int column(int ind)
{
	return ind % nCols;
}

int row(int ind)
{
	return (ind - column(ind)) / nCols;
}

int column2(int ind)
{
	return ind % nCols;
}

int row2(int ind)
{
	return (ind - column(ind)) / nCols;
}

void readBelowData(FILE * bFile, int stor[27][60], int belowLen[27])
{
  int intNum;
  for(int i = 0; i < 27; i++)
  {
    for(int j = 0; j < belowLen[i]; j++)
    {
      fscanf(bFile,"%d ",&intNum);
      stor[i][j] = intNum;
    }

    fscanf(bFile,"\n");
  }
}

// int findMin(int list)
// {
//   int min = list[0];
//   for(int i = 0; i <)
// }

int findBin(double omega)
{
  return floor(omega);
}
